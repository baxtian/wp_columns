# DESCRIPTION

Class to be inherited to extend WP Columns.

Types:
* checkbox: name, label, description
* date: name, label
* datetime: name, label
* textarea: name, label
* image: name, label, replace
* file: name, label, replace
* video: name, label, replace
* color: name, label
* select: name, label, options
* range: name, label, min, max
* integer|number: name, label

## Mantainers

Juan Sebastián Echeverry <baxtian.echeverry@gmail.com>

## Changelog

## 0.2.11

* Check warnings

## 0.2.9

* Add element name to quick and bulk edit titles

## 0.2.8

* First stable release
