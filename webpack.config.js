// Archivos a compilar
const glob = require('glob');

// Estilos y scripts base: se almacenarán en assets/(css|scripts) 
var main = glob.sync('./assets/scripts/@(*.js)')
	.reduce((acc, path) => {
		const entry = path
			.replace(/.\/assets\/scripts\/([a-z\-_]*).js/i, '$1')
		acc[entry] = path;
		return acc;
	},
		{});

// Indicar que usaremos en la variable de entorno de entradas estas tres listas.
process.env.WP_ENTRY = JSON.stringify({ ...main });

// Configuración inicial de webpack para Wordpress
const defaultConfig = require('./vendor/baxtian/merak/config/webpack.config.js');

module.exports = {
	...defaultConfig
};
