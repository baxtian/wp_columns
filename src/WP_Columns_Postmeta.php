<?php

namespace Baxtian;

use Timber\Timber;

/**
 * Clase base para los Meta de las estructuras
 */
class WP_Columns_Postmeta
{
	// Datos generales
	protected $handle; // Nombre a ser usado como identificador
	protected $prefix; // Prefijo del plugin/tema que usa esta librería
	protected $domain; // Dominio de traducción

	protected $variables       = [];
	protected $options         = [];
	protected $post_types      = [];
	protected $column_position = -1;
	protected $lang_domain     = '';
	protected $lang_path       = '';

	// Datos que se calcularán
	protected $quickedit_vars = [];
	protected $column_vars    = [];
	protected $sortable_vars  = [];

	/**
	 * Constructor de PostMeta
	 */
	public function __construct()
	{
		add_action('admin_enqueue_scripts', [$this, 'bulk_scripts'], 9);
		// add_filter('load_script_textdomain_relative_path', [$this, 'textdomain_relative_path'], 10, 2);
		
		//Activar el quickedit
		add_action('admin_footer-edit.php', [$this, 'quickedit_script']);
	}

	/**
	 * Generar los datos que deben ser calculados, declarar la funcionalidad para el
	 * Rest API, declarar el uso de edición en bloque y edición rápida, y declarar las
	 * columnas para cada estructura con la que se vincule este PostMeta.
	 */
	public function init($post_types, $handle, $variables, $column_position)
	{
		$this->post_types = $post_types;
		$this->handle = $handle;
		$this->variables = $variables;
		$this->column_position = $column_position;

		// El handle no debe tener "_"
		$this->handle = str_replace('_', '-', $this->handle);

		// Inicializar traducción
		$this->lang_domain = 'wp_columns';
		$locale            = apply_filters('plugin_locale', determine_locale(), $this->lang_domain);

		$wp_component    = basename(WP_CONTENT_URL);
		$_path           = explode($wp_component, __DIR__);
		$this->lang_path = dirname(WP_CONTENT_DIR . $_path[1]) . '/languages/';
		$mofile          = $this->lang_path . $this->lang_domain . '-' . $locale . '.mo';

		load_textdomain($this->lang_domain, $mofile);

		// Crear el arreglo con las variables que serán editadas por quickedit
		foreach ($this->variables as $key => $item) {
			$item['name'] = ($item['name']) ?? $key;
			if ((isset($item['quickedit']) && $item['quickedit']) || (isset($item['column']) && $item['column'])) {
				$this->column_vars[$item['name']] = $item;

				if (isset($item['quickedit']) && $item['quickedit']) {
					// Solo podemos hacer bulkedit para textos y checkbox
					if (in_array($item['type'], ['url', 'text', 'string', 'checkbox', 'boolean', 'integer', 'range'])) {
						$this->quickedit_vars[$item['name']] = $item;
					}
				}

				if (!isset($item['sortable']) || $item['sortable']) {
					$this->sortable_vars[$item['name']] = $item;
				}
			}

			// Guardar las opciones si es un select
			if ($item['type'] == 'select') {
				$this->options[$item['name']] = $item['options'];
			}

			// Registrar postmeta si la estructura soporta manejo de campos propios en rest
			foreach ($this->post_types as $post_type) {
				if (post_type_supports($post_type, 'custom-fields')) {
					register_post_meta($post_type, $item['name'], [
						'show_in_rest' => true,
						'single'       => true,
						'type'         => 'string',
					]);
				}
			}
		}

		// Determinar variables y acciones para quickedit y bulkedit
		add_action('bulk_edit_custom_box', [$this, 'bulkedit'], 10, 2);
		add_action('quick_edit_custom_box', [$this, 'quickedit'], 10, 2);

		// Vincular el almacenamiento de la estructura con estas variables
		foreach ($this->post_types as $post_type) {
			// Guardar
			add_action('save_post_' . $post_type, [$this, 'save']);

			// Determninar columnas para la vista lista de admninistrador
			add_filter('manage_' . $post_type . '_posts_columns', [$this, 'columns']);
			add_action('manage_' . $post_type . '_posts_custom_column', [$this, 'columcontent'], 10, 2);

			// Determinar ordenamiento
			add_filter('manage_edit-' . $post_type . '_sortable_columns', [$this, 'sortable_columns']);
			add_action('pre_get_posts', [$this, 'orderby_columns'], 1);
		}
	}

	/**
	 * Función para declarar los scripts de edición en bloque de es este TermMeta.
	 * @param  string $hook Gancho
	 */
	public function bulk_scripts($hook)
	{
		// Directorio de assets
		$wp_component = basename(WP_CONTENT_URL);
		$path         = str_replace(DIRECTORY_SEPARATOR, '/', explode($wp_component, __DIR__));
		$url          = dirname(content_url($path[1])) . '/' ;

		//Solo activar si estamos en un post_type para esta caja
		$screen = get_current_screen();
		if (!in_array($screen->post_type, $this->post_types)) {
			return;
		}

		//Registrar quickedit
		$asset_file   = include(dirname(__DIR__) . "/build/quick_edit.asset.php");
		wp_register_script('postmeta_quick_edit', $url . 'build/quick_edit.js', $asset_file['dependencies'], $asset_file['version']);
	}

	// /**
	//  * Corrige la URL relatica del script para ser empleado con el traductor de idioma.
	//  * @param  string $relative URL relativa al script con los textos a ser traducidos
	//  * @param  string $src      URL completa del script
	//  * @return string           URL relativa corregida
	//  */
	// public function textdomain_relative_path($relative, $src)
	// {
	// 	// Hacerle creer a WP que el relativo empieza en la raiz de esta
	// 	// librería y no en el vendor de quien llama a esta librería.
	// 	$relative = str_replace('vendor/baxtian/wp_columns/build/', 'build/', $relative);

	// 	return $relative;
	// }

	/**
	 * Función para agregar las columnas al quickedit
	 * @param  string $column_name Nombre de la columna a ser agregada
	 * @param  string $post_type   Nombre del tipo de estructura
	 */
	public function quickedit($column_name, $post_type)
	{
		// Recorrer la lista de términos asignado para quickedit
		foreach ($this->quickedit_vars as $key => $item) {
			// Si la columna coincide con el elemento que estamos analizando
			if ($item['name'] == $column_name) {
				// Renderizar los datos del item
				$args['element'] = $item;

				// Twig
				$this->render('partial/quickedit.twig', $args);

				// Agregar los datos de esta variable para el javascript
				add_filter('quickedit_vars', function ($quickedit_vars) {
					return array_merge($quickedit_vars, $this->quickedit_vars);
				});

				// Ya no necesitamos buscar más
				break;
			}
		}
	}

	//Función para incluir el scrit de quick_edit en caso de estar editando
	public function quickedit_script()
	{
		$screen = get_current_screen();
		if ($screen->parent_base == 'edit') {
			$post_type = (isset($_GET['post_type'])) ? $_GET['post_type'] : 'post';
			if (in_array($post_type, $this->post_types)) {
				$quickedit_vars = apply_filters('quickedit_vars', []);

				//Si hay variables declaradas, ejecutar el script
				if (count($quickedit_vars) > 0) {
					wp_localize_script('postmeta_quick_edit', 'quick_edit', ['vars' => json_encode($quickedit_vars)]);
					wp_print_scripts('postmeta_quick_edit');
				}
			}
		}
	}

	/**
	 * Renderizar la columna en el editor de quickedit
	 * @param  string $column_name Nombre de la columna a ser agregada
	 * @param  string $post_type   Nombre del tipo de estructura
	 */
	public function bulkedit($column_name, $post_type)
	{
		// Recorrer la lista de términos asignado para quickedit
		$element = false;
		foreach ($this->quickedit_vars as $key => $item) {
			// Si la columna coincide con el elemento que estamos analizando
			if ($item['name'] == $column_name) {
				// Renderizar los datos del item
				$args['element'] = $item;

				// Twig
				$this->render('partial/bulkedit.twig', $args);

				// Ya no necesitamos buscar más
				break;
			}
		}
	}

	/**
	 * Sanitizar el texto según el tipo
	 *
	 * @param string $val
	 * @param string $key
	 * @param string $handle
	 * @param integer $post_id
	 * @param string $type
	 * @return string
	 */
	private function sanitize($val, $key, $handle, $post_id, $type)
	{
		if ($type == 'textarea') {
			return apply_filters('postmeta_value', sanitize_textarea_field($val), $key, $handle, $post_id);
		}

		return apply_filters('postmeta_value', sanitize_text_field($val), $key, $handle, $post_id);
	}

	/**
	 * Guardar los datos de este PostMeta
	 * @param  int $post_id ID de la estructura que se está almacenando
	 */
	public function save($post_id)
	{
		global $pla_save_flag, $post;

		// Verificar que no sea una rutina de autoguardado.
		// Si es así, el formulario no ha sido enviado y por lo tanto cancelamos
		// el proceso.
		if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
			return;
		}

		// Revisar permisos del usuario
		if (!current_user_can('edit_post', $post_id)) {
			return;
		}

		// ------------ Almacenar variables
		// Si estamos editando desde la vista de edición rápida
		// Esto se hace porque hay campos en el formulario de edición que 
		// no están en el de edición rápida		
		if (isset($_POST['action']) && $_POST['action'] == 'inline-save') {
			// Obtener el valor de la variable desde el formulario
			// Solo usar las variables quick_edit
			foreach ($this->quickedit_vars as $key => $item) {
				$value = $this->sanitize($_POST[$key], $key, $this->handle, $post_id, $item['type']);
				update_post_meta($post_id, $key, $value);
			}

			// Si estamos editando desde la edición en lote
		} elseif (isset($_GET['bulk_edit'])) {
			// Obtener el valor de la variable desde el formulario
			foreach ($this->quickedit_vars as $key => $item) {
				$$key = $this->sanitize($_GET[$key], $key, $this->handle, $post_id, $item['type']);
			}

			$lote = $_GET['post'];
			foreach ($lote as $post_id) {
				foreach ($this->quickedit_vars as $key => $item) {
					if ($item['type'] == 'checkbox' || $item['type'] == 'boolean') {
						// Solo aplicar si se declara algún cambio
						if ($_GET[$key] != -1) {
							update_post_meta($post_id, $key, ($$key == 1));
						}
					} else {
						// Solo aplicar si se declara algún cambio
						if (!empty($$key)) {
							update_post_meta($post_id, $key, $$key);
						}
					}
				}
			}
		}
	}

	/**
	 * Determinar las columnas a ser visualizadas en el visor de la lista de cada estructura
	 * a la que fue vinculada este PostMeta
	 * @param  array $columns Lista de columnas que actualmente está asignada a la estructura
	 * @return array          Lista con las nuevas columnas
	 */
	public function columns($columns)
	{
		// Determinar las nuevas columnas
		$new_columns = [];
		foreach ($this->column_vars as $key => $item) {
			$new_columns[$key] = empty($item['label']) ? $item['description'] : $item['label'];
		}

		// Determinar posición en que irá la columna
		$pos = $this->column_position;
		if ($this->column_position < 0) {
			$pos = count($columns) + $this->column_position;
		}

		// Mezclar las columnas
		$columns = array_merge(
			array_slice($columns, 0, $pos),
			$new_columns,
			array_slice($columns, $pos)
		);

		// Aplica filro para ordenamiento de columnas
		$columns = apply_filters('wp_postmeta_columns_' . $this->handle, $columns);

		// Retornar columnas
		return $columns;
	}

	/**'postmeta'
	 * Contenido a mostrar en cada columna
	 * @param  string $content Contenido actual
	 * @param  string $column  Slug de la columna
	 * @param  int    $term_id ID del term que se está visualizando
	 */
	public function columcontent($column, $post_id)
	{
		$vars = $this->column_vars;
		if (isset($vars[$column])) {
			$var = $vars[$column];

			// Aplica filro para el contenido de la columna
			if (!is_null($txt = apply_filters('wp_postmeta_column_content_' . $this->handle, null, $column, $post_id))) {
				echo $txt;

				return;
			}

			switch ($var['type']) {
				case 'color':
					$data = get_post_meta($post_id, $column, true);
					if (!empty($data)) {
						printf('<div class="termmeta-color-block" data-%s="%2$s" style="background-color: %2$s;"></div>', $column, $data);
					}

					break;
				case 'image':
					$imagen_logo = get_post_meta($post_id, $column, true);
					if ($imagen_logo) {
						$id  = (is_array($imagen_logo)) ? $imagen_logo['id'] : $imagen_logo;
						$src = wp_get_attachment_image_src($id, 'medium');
						if (!empty($src)) {
							printf('<img class="termmeta-image-block" style="width: 100px; height: auto;" data-%s="%s" src="%s">', $column, $id, $src[0]);
						}
					}

					break;
				case 'file':
				case 'video':
					$file = get_post_meta($post_id, $column, true);
					if ($file) {
						$id  = (is_array($file)) ? $file['id'] : $file;
						$src = wp_get_attachment_url($id);
						if (!empty($src)) {
							$title = get_the_title($id);
							printf('<a href="%s" target="_BLANK">%s</a>', $src, $title);
						}
					}

					break;
				case 'checkbox':
				case 'boolean':
					$content = (get_post_meta($post_id, $column, true)) ? "<i class='dashicons dashicons-yes' data-" . $column . "='1'></i>" : "<i class='dashicons dashicons-no-alt' data-" . $column . "='0'></i>";
					echo $content;

					break;
				case 'select':
					$data    = get_post_meta($post_id, $column, true);
					$options = $this->options[$column];
					$answer  = '';
					foreach ($options as $option) {
						if ($option['value'] == $data) {
							$answer = $option['label'];
						}
					}
					echo $answer;

					break;
				case 'url':
					$data = get_post_meta($post_id, $column, true);
					if (!empty($data)) {
						printf('<a data-%s="%2$s" target="_BLANK" href="%2$s">%2$s</a>', $column, $data);
					}

					break;
				default:
					$data = get_post_meta($post_id, $column, true);
					if (!empty($data)) {
						printf('<span data-%s="%2$s">%2$s</span>', $column, $data);
					}

					break;
			}
		}
	}

	/**
	 * Determinar los campos por los que se puede ordenar
	 * @param  array $post_columns Columnas por las que se puede ordenar actualmente
	 * @return array               Arreglo de columnas incluyendo las que se defineron como ordenables en este PostMeta
	 */
	public function sortable_columns($post_columns)
	{
		// Determinar las columnas ordenables
		$new_columns = [];
		foreach ($this->sortable_vars as $key => $item) {
			$new_columns[$key] = $key;
		}

		// Agregar los campos propios de esta estructura y que estarán
		// disponibles para el ordenamiento de la lista
		$post_columns = array_merge(
			$post_columns,
			$new_columns
		);

		// Retornar nuevas columnas ordenables
		return $post_columns;
	}

	/**
	 * Ordenar según los campos propios de este TermMeta
	 * @param  WP_Query $query Instancia de consulta
	 */
	public function orderby_columns($query)
	{
		// Determinar cuál va a ser el termmeta por el cuál se está ordenando
		$orderby_items = [];
		foreach ($this->sortable_vars as $key => $item) {
			$orderby_items[] = $item['name'];
		}

		// Correr solo en el ordenamiento principal y si está solicitando 'orderby'
		if ($query->is_main_query() && ($orderby = $query->get('orderby'))) {
			if (in_array($orderby, $orderby_items)) {
				// Determinar el meta_key para la búsqueda
				$query->set('meta_key', $orderby);
				// Determinar que el tipo de ordenamiento se hará por la variable
				// asignada en meta_key. Por usar 'meta_value' el ordenamiento es
				// alfabético. Para ordenamiento numérico se usa 'meta_value_num'
				$query->set('orderby', 'meta_value');
			}
		}
	}

	/**
	 * Función para renderizar una plantilla.
	 * Requiere tener activo el plugin Timber.
	 * Lea la documentación de Timber en http://timber.github.io/timber/
	 * Timber está basado en Twig.
	 * Lee la documentación de Twig en https://twig.sensiolabs.org/doc/2.x/
	 * @param  string  $tpl   Plantilla twig a ser usada
	 * @param  array   $args  Arregloc on los argumentos a ser enviados a la plantilla
	 * @param  boolean $echo  Si desea capturar la salida del render use el parámetro echo. Si es false devolverá
	 * 						  la información directamente en vez de imprimirla. Por defecto es true.
	 * @return boolean|string Si echo es true indica si pudo o no renderizar la plantilla. Si es false retornará la
	 * 						  cadena con el texto renderizado o false si no pudo ejecutar la acción
	 */
	private function render($tpl, $args, $echo = true)
	{
		// Obtener contexto para timber
		$context = Timber::context();

		// Incluir el directorio de este elemento en Timber 
		$filename = dirname(__DIR__, 1) . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . $tpl;

		// Render
		if ($echo) {
			return Timber::render($filename, array_merge($context, $args));
		} 
		
		// ¿o devolver la cadena?
		return Timber::compile($filename, array_merge($context, $args));
	}
}
